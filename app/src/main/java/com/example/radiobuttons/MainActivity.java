package com.example.radiobuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioButton mRadioBotaoEscolhido;

    private RadioGroup mRadioGroup;

    private Button mBotao;

    private TextView mResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mRadioGroup = findViewById(R.id.RadioGroupID);
        mBotao = findViewById(R.id.botaoID);
        mResultado = findViewById(R.id.ResultadoID);

        mBotao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idRadioButton = mRadioGroup.getCheckedRadioButtonId();

             if(idRadioButton > 0){
                 mRadioBotaoEscolhido = findViewById(idRadioButton);
                 mRadioBotaoEscolhido.getText();
                 mResultado.setText(mRadioBotaoEscolhido.getText());


             }

            }
        });

    }
}
